import freezegun as freezegun
import pytest
from fastapi.testclient import TestClient

from main import app

client = TestClient(app)


@pytest.mark.vcr()
@freezegun.freeze_time("2022-08-25 08:13:10")
def test_get_stop():
    response = client.get("/stop/290", headers={"Authorization": "Basic token"})
    assert response.status_code == 200
    assert response.json() == {
        "passages": [
            {
                "ligne": "37",
                "delais": ["Passé", "10 min"],
                "destination": {"id": 46642, "name": "Charpennes"},
            },
            {
                "ligne": "C17",
                "delais": ["10 min", "20 min"],
                "destination": {"id": 46644, "name": "Charpennes"},
            },
        ],
        "stop": {"id": 290, "name": "Buers - Salengro"},
    }
