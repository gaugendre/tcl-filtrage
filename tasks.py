from invoke import task


@task
def run(ctx):
    ctx.run("uvicorn main:app --reload", pty=True, echo=True)


@task
def test(ctx):
    ctx.run("pytest", pty=True, echo=True)


@task
def deploy(ctx):
    ctx.run("ssh ubuntu /mnt/data/tcl-filtrage/update", pty=True, echo=True)
